function Car() {
    this.__constructor();

    this.steer = function(){
        console.log('Steering...')
    }
}

Car.prototype = new Transport();
Car.prototype.constructor = Car;
Car.prototype.__constructor = Transport;

Car.prototype.run = function (speed, temp) {
    this.steer();
    Transport.prototype.run.apply(this, arguments)
};

var car = new Car();
car.run();